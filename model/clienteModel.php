<?php
require_once ('../db/DBManager.php');
use DBManager;

function getImage(){
    $manager = new DBManager();

    try {
        $sql = 'SELECT imagen FROM cliente WHERE id = 3';
        $stmt = $manager ->getConexion()->prepare($sql);
        $stmt -> execute();
        $result = $stmt -> fetchAll(PDO::FETCH_ASSOC);

        return $result[0]['imagen'];
    }catch (PDOException $e){
        echo $e-> getMessage();
    }
}

function updateCliente($imagen){

    $manager = new DBManager();

    try {
        $sql = 'UPDATE cliente SET imagen =:img WHERE id = 3';
        $stmt = $manager ->getConexion()->prepare($sql);
        $stmt -> bindValue(':img', $imagen, PDO::PARAM_LOB);
        $stmt -> execute();
    }catch (PDOException $e){
        echo $e-> getMessage();
    }
}

function insertCleinte($cliente){
     $manager = new DBManager();
    try {
        $sql = "INERT INTO cliente (nombre,apellidos,fecha,sexo,telefono,dni,email,password) VALUES(:nombre,:apellidos,:fecha,:sexo,:telefono,:dni,:email,:password)";
        $nombre = $cliente->getNombre();
        $apellidos = $cliente->getApellidos();
        $fecha = $cliente->getFecha();
        $sexo = $cliente->getSexo();
        $telefono = $cliente->getTelefono();
        $dni = $cliente->getDni();
        $email = $cliente->getEmail();
        $password = password_hash($cliente -> getPassword(),PASSWORD_DEFAULT,['cost' => 10]);

        $stmt = $manager-> getConexion()->prepare($sql);
        $stmt->bindParam(':nombre',$apellidos);
        $stmt->bindParam(':fecha',$fecha);
        $stmt->bindParam(':sexo',$sexo);
        $stmt->bindParam(':telefono',$telefono);
        $stmt->bindParam(':dni',$dni);
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':password',$password);

        if ($stmt->execute()){
            echo  "todo ok";
        }else{
            echo "MAL";
        }

        if ( password_verify('123456','$2y$10$xA/vyZ8Yn8hmpPyHnLwNe.GfZxj8bc.ZchHW6PwL9EzFb0AW0wUYS') ){
            echo 'Iguales<br/>';
        }else{
            echo 'Diferentes<br/>';
        }


    }catch (PDOException $e){
        echo $e->getMessage();
    }
}
?>